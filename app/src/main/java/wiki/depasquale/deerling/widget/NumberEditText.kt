package wiki.depasquale.deerling.widget

import android.content.Context
import android.text.TextWatcher
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.EditText
import android.widget.FrameLayout
import android.widget.TextView
import androidx.core.content.res.use
import wiki.depasquale.deerling.R

class NumberEditText @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    style: Int = 0
) : FrameLayout(context, attrs, style), View.OnClickListener {

    init {
        LayoutInflater.from(context)
            .inflate(R.layout.view_number_input, this, true)
    }

    // ---

    private val labelView = findViewById<TextView>(R.id.label)
    private val countView = findViewById<EditText>(R.id.count)
    private val addView = findViewById<View>(R.id.add)
    private val subtractView = findViewById<View>(R.id.subtract)

    // ---

    var count: Int
        get() = countView.text.toString().toIntOrNull() ?: 0
        set(value) {
            countView.setTextKeepState(value.coerceAtLeast(0).toString())
        }

    var text: String
        get() = countView.text.toString()
        set(value) {
            countView.setTextKeepState(value)
        }

    var label: String
        get() = labelView.text.toString()
        set(value) {
            labelView.text = value
        }

    // ---

    init {
        context.obtainStyledAttributes(attrs, R.styleable.NumberEditText).use {
            text = it.getString(R.styleable.NumberEditText_android_text)
                ?.takeIf { it.isNotEmpty() } ?: "0"
            label = it.getString(R.styleable.NumberEditText_android_label).orEmpty()
        }

        addView.setOnClickListener(this)
        subtractView.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id ?: return) {
            R.id.add -> count++
            R.id.subtract -> count--
        }
    }

    // ---

    fun setOnTextChangedListener(listener: TextWatcher) {
        countView.addTextChangedListener(listener)
    }

}