package wiki.depasquale.deerling.core.auth

import android.accounts.Account
import android.content.Context
import com.skoumal.grimoire.talisman.seal.getOrThrow
import com.skoumal.grimoire.talisman.seal.map
import com.skoumal.grimoire.talisman.seal.onFailure
import com.skoumal.grimoire.talisman.seal.runSealed
import com.skoumal.grimoire.wand.wizard.Wizard
import com.skoumal.grimoire.wand.wizard.WizardRegistry
import com.skoumal.grimoire.wand.wizard.exists
import wiki.depasquale.data.preferences.Preferences
import wiki.depasquale.deerling.R

class AuthManager(
    context: Context,
    private val preferences: Preferences
) {

    private val accountType = context.getString(R.string.default_account_type)
    private val registry = WizardRegistry.Builder(context)
        .setAccountType(accountType)
        .build()

    val authority = context.getString(R.string.default_content_authority)

    val account: Account
        @Throws(LoggedOutException::class)
        get() {
            val account = registry.runSealed { requireNotNull(getWizard()) }
                .map { it.asAccount() }
                .onFailure { throw LoggedOutException() }
                .getOrThrow()

            if (!preferences.isValid) {
                throw LoggedOutException()
            }

            return account
        }

    val isLoggedIn
        get() = registry.exists() && preferences.isValid

    // ---

    fun logIn(name: String) {
        val wizard = Wizard(name, name)
        registry.putWizard(wizard)
    }

    fun logOut() {
        registry.removeWizard(null)
    }

    // ---

    private fun Wizard.asAccount(): Account {
        return Account(name, accountType)
    }

    // ---

    class LoggedOutException : RuntimeException()

}