package wiki.depasquale.deerling.core.decorator

import androidx.navigation.NavGraph
import androidx.navigation.NavInflater
import wiki.depasquale.deerling.core.auth.AuthManager
import wiki.depasquale.deerling.core.model.NavGraphState
import java.util.*

class NavigationGraphDecorator private constructor(
    private val graph: Int,
    private val states: Map<NavGraphState, Int>
) {

    fun create(
        inflater: NavInflater,
        manager: AuthManager
    ): NavGraph {
        val graph = inflater.inflate(graph)
        val destination = when {
            manager.isLoggedIn -> states[NavGraphState.LoggedIn]
            else -> states[NavGraphState.Default]
        }

        if (destination != null) {
            graph.startDestination = destination
        }

        return graph
    }

    // ---

    class Builder {

        private var navRes: Int = 0
        private val states: MutableMap<NavGraphState, Int> = EnumMap(NavGraphState::class.java)

        fun setGraph(navRes: Int) = apply {
            this.navRes = navRes
        }

        fun setStateDestination(state: NavGraphState, destination: Int) = apply {
            this.states[state] = destination
        }

        fun build(): NavigationGraphDecorator {
            return NavigationGraphDecorator(
                graph = navRes,
                states = states
            )
        }

    }

}