package wiki.depasquale.deerling.core.loader

import android.Manifest
import android.content.Context
import android.location.Location
import android.os.Looper
import androidx.annotation.RequiresPermission
import com.google.android.gms.location.*
import com.skoumal.grimoire.talisman.seal.Seal
import com.skoumal.grimoire.talisman.seal.getOrThrow
import com.skoumal.grimoire.talisman.seal.onFailure
import com.skoumal.grimoire.talisman.seal.runSealed
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.flow.first
import wiki.depasquale.deerling.tool.await

class LocationLoader(context: Context) {

    private val provider = LocationServices.getFusedLocationProviderClient(context)

    @RequiresPermission(Manifest.permission.ACCESS_COARSE_LOCATION)
    suspend fun requestLocation(): Seal<Location> {
        val availability = provider.locationAvailability.await()
            .onFailure { return Seal.failure(it) }
            .getOrThrow()

        return requestLocationInternal(availability?.isLocationAvailable == true)
    }

    @RequiresPermission(Manifest.permission.ACCESS_COARSE_LOCATION)
    private suspend fun requestLocationInternal(
        attemptLastLocation: Boolean
    ): Seal<Location> {

        @Throws
        suspend fun requestCurrentLocation(): Location {
            val request = LocationRequest.create()
                .setNumUpdates(1)
                .setMaxWaitTime(1000)

            return provider.requestLocationFlow(request).first()
        }

        if (attemptLastLocation) {
            val lastLocation = provider.lastLocation.await().getOrNull()
            if (lastLocation != null) {
                return Seal.success(lastLocation)
            }
        }

        return runSealed { requestCurrentLocation() }
    }

    @RequiresPermission(Manifest.permission.ACCESS_COARSE_LOCATION)
    private fun FusedLocationProviderClient.requestLocationFlow(request: LocationRequest): Flow<Location> {
        return callbackFlow {
            val callback = object : LocationCallback() {
                override fun onLocationResult(p0: LocationResult) {
                    trySend(p0.lastLocation)
                }
            }
            requestLocationUpdates(request, callback, Looper.getMainLooper())
            awaitClose {
                removeLocationUpdates(callback)
            }
        }
    }

}