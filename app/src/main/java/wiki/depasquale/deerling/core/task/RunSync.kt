package wiki.depasquale.deerling.core.task

import android.content.ContentResolver
import android.content.Context
import com.skoumal.grimoire.talisman.UseCase
import wiki.depasquale.deerling.core.auth.AuthManager
import wiki.depasquale.deerling.core.sync.FlightSyncAdapter

class RunSync internal constructor(
    private val context: Context,
    private val auth: AuthManager,
) : UseCase<Unit, Unit> {

    override suspend fun use(input: Unit) {
        val request = FlightSyncAdapter.getSyncRequest(context.resources, auth)

        val account = auth.account
        val authority = auth.authority

        ContentResolver.setIsSyncable(account, authority, 1)
        ContentResolver.setMasterSyncAutomatically(true)
        ContentResolver.setSyncAutomatically(account, authority, true)
        ContentResolver.requestSync(request)
    }

}