package wiki.depasquale.deerling.core.sync

import android.accounts.Account
import android.content.*
import android.content.res.Resources
import android.os.Bundle
import com.skoumal.grimoire.talisman.invoke
import com.skoumal.grimoire.talisman.seal.onFailure
import kotlinx.coroutines.runBlocking
import org.koin.core.component.inject
import timber.log.Timber
import wiki.depasquale.data.task.UpdateFlights
import wiki.depasquale.deerling.R
import wiki.depasquale.deerling.core.auth.AuthManager
import wiki.depasquale.deerling.di.Component

class FlightSyncAdapter @JvmOverloads constructor(
    context: Context,
    autoInitialize: Boolean,
    allowParallelSyncs: Boolean = false,
) : AbstractThreadedSyncAdapter(context, autoInitialize, allowParallelSyncs) {

    private val flights: UpdateFlights by Component.inject()

    override fun onPerformSync(
        account: Account?,
        extras: Bundle?,
        authority: String?,
        provider: ContentProviderClient?,
        syncResult: SyncResult?
    ) {
        runBlocking { flights().onFailure { Timber.e(it) } }
    }

    companion object {

        @Throws(AuthManager.LoggedOutException::class)
        fun getSyncRequest(
            resources: Resources,
            auth: AuthManager
        ): SyncRequest {
            val account = auth.account
            return SyncRequest.Builder()
                .setExpedited(true)
                .setIgnoreBackoff(true)
                .setManual(true)
                .setExtras(Bundle.EMPTY)
                .setSyncAdapter(account, resources.getString(R.string.default_content_authority))
                .build()
        }

    }

}