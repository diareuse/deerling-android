package wiki.depasquale.deerling.core.model

enum class NavGraphState {

    Default,
    LoggedIn;

}