package wiki.depasquale.deerling.core.writer

import wiki.depasquale.data.preferences.Preferences
import wiki.depasquale.deerling.core.auth.AuthManager
import wiki.depasquale.deerling.ui.intro.IntroState

class IntroStateWriter(
    private val auth: AuthManager,
    private val preferences: Preferences
) {

    @Throws
    fun write(state: IntroState) {
        auth.logIn(state.name)
        with(preferences) {
            children = state.children.toInt()
            infants = state.infants.toInt()
            adults = state.adults.toInt()
            latitude = state.latitude.toDouble()
            longitude = state.longitude.toDouble()
            radius = 250.0
        }
    }

}