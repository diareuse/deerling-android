package wiki.depasquale.deerling.startup

import android.content.Context
import androidx.startup.Initializer
import org.koin.android.ext.koin.androidContext
import org.koin.core.KoinApplication
import org.koin.core.context.startKoin
import wiki.depasquale.data.di.dataModule
import wiki.depasquale.data.di.networkModule
import wiki.depasquale.data.di.persistenceModule
import wiki.depasquale.deerling.di.appModule

class DIInitializer : Initializer<KoinApplication> {

    override fun create(context: Context): KoinApplication {
        return startKoin {
            androidContext(context)
            modules(appModule, dataModule, networkModule, persistenceModule)
        }
    }

    override fun dependencies(): List<Class<out Initializer<*>>> {
        return emptyList()
    }

}