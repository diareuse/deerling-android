package wiki.depasquale.deerling.startup

import android.content.Context
import androidx.startup.Initializer
import timber.log.Timber
import wiki.depasquale.deerling.BuildConfig

class LoggingInitializer : Initializer<Unit> {

    override fun create(context: Context) {
        val tree = when (BuildConfig.DEBUG) {
            true -> Timber.DebugTree()
            else -> return
        }
        Timber.plant(tree)
    }

    override fun dependencies(): List<Class<out Initializer<*>>> {
        return emptyList()
    }

}