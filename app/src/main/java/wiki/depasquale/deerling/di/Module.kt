package wiki.depasquale.deerling.di

import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import wiki.depasquale.deerling.core.auth.AuthManager
import wiki.depasquale.deerling.core.loader.LocationLoader
import wiki.depasquale.deerling.core.task.RunSync
import wiki.depasquale.deerling.core.writer.IntroStateWriter
import wiki.depasquale.deerling.ui.MainActivity
import wiki.depasquale.deerling.ui.MainViewModel
import wiki.depasquale.deerling.ui.home.HomeViewModel
import wiki.depasquale.deerling.ui.intro.IntroFragment
import wiki.depasquale.deerling.ui.intro.IntroViewModel

val appModule = module {

    single { AuthManager(get(), get()) }

    viewModel { MainViewModel() }
    viewModel { HomeViewModel(get(), get(), get()) }
    viewModel { IntroViewModel() }

    scope<IntroFragment> {
        scoped { LocationLoader(get()) }
        scoped { IntroStateWriter(get(), get()) }
    }

    scope<MainActivity> {
        factory { RunSync(get(), get()) }
    }

}