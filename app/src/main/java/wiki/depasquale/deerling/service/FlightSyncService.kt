package wiki.depasquale.deerling.service

import com.skoumal.grimoire.wand.wizard.sync.SyncService
import wiki.depasquale.deerling.core.sync.FlightSyncAdapter

class FlightSyncService : SyncService() {
    override fun onCreateSyncAdapter() = FlightSyncAdapter(this, true)
}