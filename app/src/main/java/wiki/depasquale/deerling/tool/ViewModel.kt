package wiki.depasquale.deerling.tool

import androidx.lifecycle.ViewModel
import com.skoumal.grimoire.wand.recyclerview.ExtrasBinder
import wiki.depasquale.deerling.BR

fun ViewModel.binder() = ExtrasBinder(
    onBindExtras = { it.setVariable(BR.viewModel, this) },
    onClearExtras = { it.setVariable(BR.viewModel, this) },
)