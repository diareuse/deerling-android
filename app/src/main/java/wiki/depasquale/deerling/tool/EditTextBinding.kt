package wiki.depasquale.deerling.tool

import android.text.Editable
import android.text.TextWatcher
import androidx.databinding.BindingAdapter
import androidx.databinding.InverseBindingAdapter
import androidx.databinding.InverseBindingListener
import wiki.depasquale.deerling.widget.NumberEditText

@BindingAdapter("android:text")
fun NumberEditText.setText(text: String?) {
    this.text = text.orEmpty()
}

@BindingAdapter("android:textAttrChanged")
fun NumberEditText.setOnTextChangedListener(listener: InverseBindingListener) {
    setOnTextChangedListener(object : TextWatcher {
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) = Unit
        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) = Unit
        override fun afterTextChanged(s: Editable?) {
            listener.onChange()
        }
    })
}

@InverseBindingAdapter(attribute = "android:text", event = "android:textAttrChanged")
fun NumberEditText.getText() = text