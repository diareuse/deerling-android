package wiki.depasquale.deerling.tool

import com.google.android.gms.tasks.Task
import com.skoumal.grimoire.talisman.seal.Seal
import kotlinx.coroutines.suspendCancellableCoroutine
import kotlin.coroutines.resume

suspend fun <Result> Task<Result>.await(): Seal<Result> = suspendCancellableCoroutine { cont ->
    addOnCompleteListener { task ->
        if (cont.isCompleted) {
            return@addOnCompleteListener
        }
        when (task.isSuccessful) {
            true -> cont.resume(Seal.success(task.result))
            false -> cont.resume(Seal.failure(task.exception ?: RuntimeException()))
        }
    }
}