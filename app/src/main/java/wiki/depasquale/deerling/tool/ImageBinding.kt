package wiki.depasquale.deerling.tool

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import coil.load

@BindingAdapter("android:image")
fun ImageView.setImage(source: String?) {
    load(source)
}