package wiki.depasquale.deerling.tool

import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.core.view.updateLayoutParams
import androidx.databinding.BindingAdapter

@BindingAdapter("android:layout_marginLeft")
fun View.updateMarginLeft(margin: Int) {
    updateLayoutParams<ViewGroup.MarginLayoutParams> {
        leftMargin = margin
    }
}

@BindingAdapter("android:layout_marginRight")
fun View.updateMarginRight(margin: Int) {
    updateLayoutParams<ViewGroup.MarginLayoutParams> {
        rightMargin = margin
    }
}

@BindingAdapter("android:layout_marginTop")
fun View.updateMarginTop(margin: Int) {
    updateLayoutParams<ViewGroup.MarginLayoutParams> {
        topMargin = margin
    }
}

@BindingAdapter("android:layout_marginBottom")
fun View.updateMarginBottom(margin: Int) {
    updateLayoutParams<ViewGroup.MarginLayoutParams> {
        bottomMargin = margin
    }
}

// ---

@BindingAdapter("gone")
fun View.setGone(isGone: Boolean) {
    isVisible = !isGone
}