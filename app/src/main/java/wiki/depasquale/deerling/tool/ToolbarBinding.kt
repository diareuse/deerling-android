package wiki.depasquale.deerling.tool

import android.view.View
import androidx.appcompat.widget.Toolbar
import androidx.databinding.BindingAdapter

@BindingAdapter("navigationIconClick")
fun Toolbar.setNavigationIconClickListener(listener: View.OnClickListener?) {
    setNavigationOnClickListener(listener)
}