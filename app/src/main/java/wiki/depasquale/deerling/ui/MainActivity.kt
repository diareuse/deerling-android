package wiki.depasquale.deerling.ui

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavController
import androidx.navigation.navOptions
import com.skoumal.grimoire.talisman.invoke
import com.skoumal.grimoire.transfusion.live.viewBinding
import com.skoumal.grimoire.transfusion.method.Cell
import com.skoumal.grimoire.transfusion.method.InActivity
import org.koin.android.ext.android.inject
import org.koin.android.scope.AndroidScopeComponent
import org.koin.androidx.scope.activityScope
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber
import wiki.depasquale.arch.BaseActivity
import wiki.depasquale.deerling.R
import wiki.depasquale.deerling.core.auth.AuthManager
import wiki.depasquale.deerling.core.decorator.NavigationGraphDecorator
import wiki.depasquale.deerling.core.model.NavGraphState
import wiki.depasquale.deerling.core.task.RunSync
import wiki.depasquale.deerling.databinding.ActivityMainBinding

class MainActivity : BaseActivity(R.layout.activity_main), AndroidScopeComponent {

    override val binding: ActivityMainBinding by viewBinding()
    override val viewModel: MainViewModel by viewModel()
    override val scope by activityScope()

    private val manager: AuthManager by inject()
    private val sync: RunSync by scope.inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        createNavGraph(navController)
    }

    override fun onResume() {
        super.onResume()

        performSync()
    }

    // ---

    private fun createNavGraph(navController: NavController) {
        val decorator = NavigationGraphDecorator.Builder()
            .setGraph(R.navigation.nav_graph)
            .setStateDestination(NavGraphState.LoggedIn, R.id.homeFragment)
            .setStateDestination(NavGraphState.Default, R.id.introFragment)
            .build()

        navController.graph = decorator.create(
            inflater = navController.navInflater,
            manager = manager
        )
    }

    // ---

    private fun navigateToIntro() {
        val controller = navController
        val destination = controller.currentDestination?.id
        if (destination == R.id.introFragment) {
            return
        }

        val options = navOptions {
            popUpTo(R.id.homeFragment) {
                inclusive = true
            }
        }
        controller.navigate(R.id.introFragment, null, options)
    }

    private fun performSync() {
        lifecycleScope.launchWhenCreated {
            val error = sync().throwableOrNull()
            if (error is AuthManager.LoggedOutException) {
                navigateToIntro()
            }
            Timber.e(error)
        }
    }

    // ---

    class Refresh : Cell(), InActivity {
        override fun invoke(activity: ComponentActivity) {
            if (activity !is MainActivity) {
                return
            }

            activity.performSync()
        }
    }

}