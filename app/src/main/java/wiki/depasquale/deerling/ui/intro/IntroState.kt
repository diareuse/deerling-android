package wiki.depasquale.deerling.ui.intro

interface IntroState {
    val name: String
    val infants: String
    val children: String
    val adults: String
    val latitude: String
    val longitude: String
}