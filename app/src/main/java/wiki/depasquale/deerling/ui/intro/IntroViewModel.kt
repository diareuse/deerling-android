package wiki.depasquale.deerling.ui.intro

import androidx.databinding.Bindable
import com.skoumal.grimoire.transfusion.observable
import wiki.depasquale.arch.BaseViewModel
import wiki.depasquale.arch.cell.SnackbarCell
import wiki.depasquale.deerling.BR
import wiki.depasquale.deerling.ui.intro.IntroFragment.*

class IntroViewModel : BaseViewModel(), IntroState {

    override var name by observable("", BR.name)
        @Bindable get

    override var infants by observable("0", BR.infants)
        @Bindable get

    override var children by observable("0", BR.children)
        @Bindable get

    override var adults by observable("0", BR.adults)
        @Bindable get

    override var latitude by observable("", BR.latitude)
        @Bindable get

    override var longitude by observable("", BR.longitude)
        @Bindable get

    // ---

    private val validator = IntroValidator()

    // ---

    fun onContinueIdentity() {
        validator.validateIdentity(this)
            .onFailure { SnackbarCell(it.errors.first()).offer() }
            .onSuccess { ScrollTo(IntroPage.Passengers).offer() }
    }

    fun onContinuePassengers() {
        validator.validatePassengers(this)
            .onFailure { SnackbarCell(it.errors.first()).offer() }
            .onSuccess { ScrollTo(IntroPage.Location).offer() }
    }

    fun onContinueLocation() {
        validator.validateLocation(this)
            .onFailure { SnackbarCell(it.errors.first()).offer() }
            .onSuccess { WriteStateAndNavigate(this, IntroFragmentDirections.toHome()).offer() }
    }

    // ---

    fun onRequestPermission() {
        RequestLocationPermission().offer()
    }

}


