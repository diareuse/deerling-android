package wiki.depasquale.deerling.ui.intro

import wiki.depasquale.deerling.R

class IntroValidator {

    fun validateIdentity(state: IntroState): ValidationResult {
        if (state.name.isBlank()) {
            return ValidationResult(R.string.error_name_empty)
        }

        return ValidationResult()
    }

    fun validatePassengers(state: IntroState): ValidationResult {
        validateIdentity(state).onFailure { return it }

        val infants = state.infants.toIntOrNull() ?: 0
        val children = state.children.toIntOrNull() ?: 0
        val adults = state.adults.toIntOrNull() ?: 0

        if (infants <= 0 && children <= 0 && adults <= 0) {
            return ValidationResult(R.string.error_passengers_empty)
        }

        return ValidationResult()
    }

    fun validateLocation(state: IntroState): ValidationResult {
        validatePassengers(state).onFailure { return it }

        val latitude = state.latitude.toDoubleOrNull()
        val longitude = state.longitude.toDoubleOrNull()

        if (latitude == null || longitude == null) {
            return ValidationResult(R.string.error_location_empty)
        }

        return ValidationResult()
    }

    // ---

    data class ValidationResult(
        val errors: List<Int>
    ) {

        constructor(error: Int) : this(listOf(error))
        constructor() : this(emptyList())

        val isSuccess get() = errors.isEmpty()

        inline fun onFailure(body: (ValidationResult) -> Unit) = apply {
            if (!isSuccess) body(this@ValidationResult)
        }

        inline fun onSuccess(body: (ValidationResult) -> Unit) = apply {
            if (isSuccess) body(this@ValidationResult)
        }

    }

}