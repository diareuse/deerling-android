package wiki.depasquale.deerling.ui.intro

import wiki.depasquale.arch.adapter.GenericAdapter
import wiki.depasquale.deerling.R

enum class IntroPage(override val viewType: Int) : GenericAdapter.HasViewType {
    Identity(R.layout.item_intro_identity),
    Passengers(R.layout.item_intro_passengers),
    Location(R.layout.item_intro_location);
}