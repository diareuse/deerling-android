package wiki.depasquale.deerling.ui.home

import kotlinx.coroutines.flow.StateFlow
import wiki.depasquale.arch.adapter.GenericAdapter
import wiki.depasquale.data.model.*
import wiki.depasquale.deerling.R

data class HomeFlight(
    val flight: Flight,
    val countryFrom: StateFlow<Country>,
    val countryTo: StateFlow<Country>,
    val destinations: StateFlow<List<Destination>>,
    val routes: StateFlow<List<Route>>,
    val availability: StateFlow<Availability>,
    val bagPrice: StateFlow<BagPrice>
) : GenericAdapter.HasViewType {

    override val viewType: Int
        get() = R.layout.item_home_flight

}