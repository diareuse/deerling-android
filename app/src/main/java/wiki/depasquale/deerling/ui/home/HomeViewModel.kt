package wiki.depasquale.deerling.ui.home

import androidx.lifecycle.viewModelScope
import com.skoumal.grimoire.talisman.observe
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.stateIn
import wiki.depasquale.arch.BaseViewModel
import wiki.depasquale.arch.cell.LinkCell
import wiki.depasquale.data.model.FlightContainer
import wiki.depasquale.data.preferences.Preferences
import wiki.depasquale.data.task.GetFlights
import wiki.depasquale.deerling.core.auth.AuthManager

class HomeViewModel(
    getFlights: GetFlights,
    private val auth: AuthManager,
    private val preferences: Preferences
) : BaseViewModel() {

    val currency get() = preferences.currency
    val name get() = auth.account.name
    val items = getFlights.observe(viewModelScope)
        .map { it.asHomeFlights() }
        .stateIn(viewModelScope, SharingStarted.Lazily, emptyList())

    // ---

    fun onItemClick(item: HomeFlight) {
        LinkCell(item.flight.deepLink ?: return).offer()
    }

    // ---

    private fun List<FlightContainer>.asHomeFlights() = map {
        HomeFlight(
            flight = it.flight,
            countryFrom = it.countryFrom,
            countryTo = it.countryTo,
            destinations = it.destinations,
            routes = it.routes,
            availability = it.availability,
            bagPrice = it.bagPrice
        )
    }

}