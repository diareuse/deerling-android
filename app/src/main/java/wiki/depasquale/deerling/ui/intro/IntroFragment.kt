package wiki.depasquale.deerling.ui.intro

import android.Manifest
import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.RequiresPermission
import androidx.fragment.app.Fragment
import androidx.navigation.NavDirections
import com.google.android.material.tabs.TabLayoutMediator
import com.skoumal.grimoire.talisman.seal.map
import com.skoumal.grimoire.talisman.seal.onFailure
import com.skoumal.grimoire.talisman.seal.onSuccess
import com.skoumal.grimoire.talisman.seal.runSealed
import com.skoumal.grimoire.transfusion.live.viewBinding
import com.skoumal.grimoire.transfusion.method.Cell
import com.skoumal.grimoire.transfusion.method.InFragment
import com.skoumal.grimoire.wand.recyclerview.diff.submitList
import org.koin.android.scope.AndroidScopeComponent
import org.koin.androidx.scope.fragmentScope
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber
import wiki.depasquale.arch.BaseFragment
import wiki.depasquale.arch.adapter.GenericAdapter
import wiki.depasquale.arch.cell.NavigationCell
import wiki.depasquale.deerling.R
import wiki.depasquale.deerling.core.loader.LocationLoader
import wiki.depasquale.deerling.core.writer.IntroStateWriter
import wiki.depasquale.deerling.databinding.FragmentIntroBinding
import wiki.depasquale.deerling.tool.binder
import wiki.depasquale.deerling.ui.MainActivity

class IntroFragment : BaseFragment(R.layout.fragment_intro), AndroidScopeComponent {

    override val scope by fragmentScope()
    override val viewModel: IntroViewModel by viewModel()
    override val binding: FragmentIntroBinding by viewBinding()

    private var mediator: TabLayoutMediator? = null

    private val loader: LocationLoader by scope.inject()
    private val writer: IntroStateWriter by scope.inject()

    @SuppressLint("MissingPermission")
    private val location =
        registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) { results ->
            if (results.all { it.value }) requestLocation()
        }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupContent()
        setupTabs()
    }

    override fun onDestroyView() {
        super.onDestroyView()

        mediator?.detach()
    }

    // ---

    @RequiresPermission(Manifest.permission.ACCESS_COARSE_LOCATION)
    private fun requestLocation() {
        viewScope.launchWhenCreated {
            loader.requestLocation().onSuccess {
                viewModel.latitude = it.latitude.toString()
                viewModel.longitude = it.longitude.toString()
                viewModel.onContinueLocation()
            }.onFailure {
                Timber.e(it)
            }
        }
    }

    // ---

    private fun setupContent() {
        val adapter = GenericAdapter(viewLifecycleOwner, viewModel.binder())

        binding.content.adapter = adapter

        viewScope.launchWhenCreated {
            adapter.submitList(IntroPage.values().toList())
        }
    }

    private fun setupTabs() {
        mediator = TabLayoutMediator(
            binding.tabLayout,
            binding.content,
            true,
            true
        ) { tab, position ->
            val type = binding.content.adapter?.getItemViewType(position)
            tab.text = when (type) {
                R.layout.item_intro_location -> "Location"
                R.layout.item_intro_passengers -> "Passengers"
                R.layout.item_intro_identity -> "Identity"
                else -> null
            }
        }

        mediator?.attach()
    }

    // ---

    class ScrollTo(private val location: IntroPage) : Cell(), InFragment {
        override fun invoke(fragment: Fragment) {
            if (fragment !is IntroFragment) {
                return
            }

            val pager = fragment.binding.content
            val adapter = pager.adapter as? GenericAdapter ?: return
            val position = adapter.getItemPosition(location)
            if (position >= 0) {
                pager.setCurrentItem(position, true)
            }
        }
    }

    class RequestLocationPermission : Cell(), InFragment {
        override fun invoke(fragment: Fragment) {
            if (fragment !is IntroFragment) {
                return
            }

            val permissions = arrayOf(
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION
            )
            fragment.location.launch(permissions)
        }
    }

    class WriteStateAndNavigate(
        private val state: IntroState,
        private val directions: NavDirections
    ) : Cell(), InFragment {
        override fun invoke(fragment: Fragment) {
            if (fragment !is IntroFragment) {
                return
            }

            fragment.writer.runSealed { write(state) }
                .map { MainActivity.Refresh().invoke(fragment.requireActivity()) }
                .map { NavigationCell(directions).invoke(fragment) }
                .onFailure { Timber.e(it) }
        }
    }

}