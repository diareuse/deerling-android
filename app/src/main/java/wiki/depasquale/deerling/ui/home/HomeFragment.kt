package wiki.depasquale.deerling.ui.home

import android.os.Bundle
import android.view.View
import androidx.interpolator.view.animation.FastOutSlowInInterpolator
import androidx.recyclerview.widget.PagerSnapHelper
import com.skoumal.grimoire.transfusion.live.viewBinding
import com.skoumal.grimoire.wand.recyclerview.diff.submitList
import kotlinx.coroutines.flow.collect
import org.koin.androidx.viewmodel.ext.android.viewModel
import wiki.depasquale.arch.BaseFragment
import wiki.depasquale.arch.adapter.GenericAdapter
import wiki.depasquale.deerling.R
import wiki.depasquale.deerling.databinding.FragmentHomeBinding
import wiki.depasquale.deerling.tool.binder

class HomeFragment : BaseFragment(R.layout.fragment_home) {

    override val binding: FragmentHomeBinding by viewBinding()
    override val viewModel: HomeViewModel by viewModel()

    private val snapHelper = PagerSnapHelper()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupContent()
        setupGreeting()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        snapHelper.attachToRecyclerView(null)
    }

    // ---

    private fun setupContent() {
        val adapter = GenericAdapter(viewLifecycleOwner, viewModel.binder())

        binding.content.adapter = adapter

        snapHelper.attachToRecyclerView(binding.content)

        viewScope.launchWhenCreated {
            viewModel.items.collect {
                adapter.submitList(it)
            }
        }
    }

    private fun setupGreeting() {
        binding.greeting.viewTreeObserver.addOnGlobalLayoutListener {
            binding.greeting.animate()
                .translationY(-binding.greeting.measuredHeight.toFloat())
                .setInterpolator(FastOutSlowInInterpolator())
                .setStartDelay(3000L)
                .start()
        }
    }

}
