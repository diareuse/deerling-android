package wiki.depasquale.arch

import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavController
import androidx.navigation.Navigation.findNavController
import com.skoumal.grimoire.transfusion.method.consumer.transfusion
import com.skoumal.grimoire.wand.InsetsWand
import com.skoumal.grimoire.wand.applyWindowAppearance
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.collect

abstract class BaseActivity(@LayoutRes layoutRes: Int) : AppCompatActivity(layoutRes) {

    abstract val viewModel: BaseViewModel
    abstract val binding: ViewDataBinding

    val navController: NavController
        get() = findNavController(this, R.id.nav_controller)

    init {
        transfusion { viewModel }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // launch lifecycle tasks
        lifecycleScope.launchWhenCreated(::collectWand)

        // apply variables
        binding.setVariable(BR.viewModel, viewModel)

        // apply transparent theme
        applyWindowAppearance()
    }

    private suspend fun collectWand(scope: CoroutineScope) {
        InsetsWand(this).collect {
            viewModel.insets = it
        }
    }

}