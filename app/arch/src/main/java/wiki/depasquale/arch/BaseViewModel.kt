package wiki.depasquale.arch

import androidx.databinding.Bindable
import androidx.lifecycle.ViewModel
import androidx.navigation.NavDirections
import androidx.navigation.NavOptions
import com.skoumal.grimoire.transfusion.Observer
import com.skoumal.grimoire.transfusion.Transfusion
import com.skoumal.grimoire.transfusion.method.Cell
import com.skoumal.grimoire.transfusion.observable
import com.skoumal.grimoire.wand.InsetsWand
import wiki.depasquale.arch.cell.NavigationCell

abstract class BaseViewModel : ViewModel(),
    Transfusion<Cell> by Transfusion.getDefault(),
    Observer by Observer.getDefault() {

    var insets by observable(InsetsWand(), BR.insets)
        @Bindable get

    // ---

    fun onBackClick() =
        NavigationCell(null).offer()

    // ---

    protected fun NavDirections.offer(options: NavOptions? = null) =
        NavigationCell(this, options)

}