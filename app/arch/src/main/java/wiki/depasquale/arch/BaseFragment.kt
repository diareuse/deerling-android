package wiki.depasquale.arch

import android.os.Bundle
import android.view.View
import androidx.activity.OnBackPressedCallback
import androidx.annotation.CallSuper
import androidx.annotation.LayoutRes
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.skoumal.grimoire.transfusion.method.consumer.transfusion
import com.skoumal.grimoire.wand.InsetsWand
import com.skoumal.grimoire.wand.applyWindowAppearance
import com.skoumal.grimoire.wand.hideIME
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.collect
import wiki.depasquale.arch.cell.NavigationCell
import java.lang.ref.WeakReference

abstract class BaseFragment(
    @LayoutRes layoutRes: Int
) : Fragment(layoutRes) {

    abstract val viewModel: BaseViewModel
    abstract val binding: ViewDataBinding

    protected val navController: NavController
        get() = Navigation.findNavController(requireView())

    protected val viewScope
        get() = viewLifecycleOwner.lifecycleScope

    init {
        transfusion { viewModel }
    }

    @CallSuper
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // launch lifecycle tasks
        lifecycleScope.launchWhenCreated(::collectWand)

        // apply variables
        binding.setVariable(BR.viewModel, viewModel)

        // apply transparent theme
        activity?.applyWindowAppearance()

        // apply back press listener
        requireActivity().onBackPressedDispatcher
            .addCallback(viewLifecycleOwner, OnBackPressed(this))
    }

    override fun onDestroyView() {
        hideIME()
        super.onDestroyView()
    }

    // ---

    open fun onBackPressed() {
        NavigationCell(null).invoke(this)
    }

    private suspend fun collectWand(scope: CoroutineScope) {
        InsetsWand(this).collect {
            viewModel.insets = it
        }
    }

    // ---

    private class OnBackPressed(
        private val fragment: WeakReference<BaseFragment>
    ) : OnBackPressedCallback(true) {

        constructor(fragment: BaseFragment) : this(WeakReference(fragment))

        override fun handleOnBackPressed() {
            val fragment = fragment.get()
            if (fragment == null) {
                isEnabled = false
                return
            }

            fragment.onBackPressed()
        }

    }

}
