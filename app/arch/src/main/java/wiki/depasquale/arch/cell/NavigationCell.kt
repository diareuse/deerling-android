package wiki.depasquale.arch.cell

import androidx.fragment.app.Fragment
import androidx.navigation.NavDirections
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import com.skoumal.grimoire.transfusion.method.Cell
import com.skoumal.grimoire.transfusion.method.InFragment

class NavigationCell(
    private val directions: NavDirections?,
    private val options: NavOptions? = null
) : Cell(), InFragment {

    override fun invoke(fragment: Fragment) {
        val controller = fragment.findNavController()

        if (directions == null) {
            if (!controller.navigateUp()) {
                fragment.activity?.finishAffinity()
            }
            return
        }

        controller.navigate(directions, options)
    }

}