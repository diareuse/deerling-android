package wiki.depasquale.arch.cell

import android.view.View
import androidx.activity.ComponentActivity
import com.google.android.material.snackbar.Snackbar
import com.skoumal.grimoire.transfusion.method.Cell
import com.skoumal.grimoire.transfusion.method.InActivity
import com.skoumal.grimoire.wand.TextWand
import com.skoumal.grimoire.wand.asText
import wiki.depasquale.arch.R

class SnackbarCell(
    private val text: TextWand,
    private val duration: Int
) : Cell(), InActivity {

    constructor(text: Int, duration: Int = Snackbar.LENGTH_LONG) : this(asText(text), duration)
    constructor(text: String, duration: Int = Snackbar.LENGTH_LONG) : this(asText(text), duration)

    override fun invoke(activity: ComponentActivity) {
        Snackbar.make(getView(activity), text.getText(activity.resources), duration).show()
    }

    private fun getView(activity: ComponentActivity): View {
        return activity.findViewById(R.id.snackbar_anchor)
            ?: activity.window.findViewById(android.R.id.content)
    }

}