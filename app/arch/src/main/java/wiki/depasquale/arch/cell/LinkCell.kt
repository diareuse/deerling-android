package wiki.depasquale.arch.cell

import android.content.Intent
import android.net.Uri
import androidx.activity.ComponentActivity
import com.skoumal.grimoire.transfusion.method.Cell
import com.skoumal.grimoire.transfusion.method.InActivity

class LinkCell(private val link: String) : Cell(), InActivity {
    override fun invoke(activity: ComponentActivity) {
        val intentView = Intent(Intent.ACTION_VIEW)
            .setData(Uri.parse(link))
        val intentPicker = Intent.createChooser(intentView, "")
        activity.startActivity(intentPicker)
    }
}