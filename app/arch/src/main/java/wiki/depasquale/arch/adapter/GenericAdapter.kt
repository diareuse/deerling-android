package wiki.depasquale.arch.adapter

import androidx.lifecycle.LifecycleOwner
import androidx.recyclerview.widget.DiffUtil
import com.skoumal.grimoire.wand.recyclerview.ExtrasBinder
import com.skoumal.grimoire.wand.recyclerview.adapter.AsyncBindingAdapter
import com.skoumal.grimoire.wand.recyclerview.diff.getItemAt

class GenericAdapter(
    lifecycleOwner: LifecycleOwner,
    extrasBinder: ExtrasBinder? = null
) : AsyncBindingAdapter<GenericAdapter.HasViewType>(differ, lifecycleOwner, extrasBinder) {

    override fun getItemViewType(position: Int): Int {
        val item = getItemAt(position)
        return requireNotNull(item).viewType
    }

    fun getItemPosition(item: HasViewType): Int {
        return currentList.indexOfFirst { it == item }
    }

    interface HasViewType {
        val viewType: Int
        override fun equals(other: Any?): Boolean
        override fun hashCode(): Int
    }

    companion object {

        private val differ = object : DiffUtil.ItemCallback<HasViewType>() {
            override fun areItemsTheSame(oldItem: HasViewType, newItem: HasViewType): Boolean {
                return oldItem === newItem
            }

            override fun areContentsTheSame(oldItem: HasViewType, newItem: HasViewType): Boolean {
                return oldItem == newItem
            }
        }

    }

}