package wiki.depasquale.persistence.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity(
    tableName = "availabilities",
    foreignKeys = [
        ForeignKey(
            entity = Flight::class,
            parentColumns = ["id"],
            childColumns = ["parent"],
            onDelete = ForeignKey.CASCADE
        )
    ]
)
data class Availability(
    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = "parent") val parent: String,
    @ColumnInfo(name = "seats") val seats: Int
)

