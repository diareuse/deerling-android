package wiki.depasquale.persistence.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity(
    tableName = "routes",
    foreignKeys = [
        ForeignKey(
            entity = Flight::class,
            parentColumns = ["id"],
            childColumns = ["parent"],
            onDelete = ForeignKey.CASCADE
        )
    ]
)
data class Route(
    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = "parent") val parent: String,
    @ColumnInfo(name = "route") val route: List<String>
)