package wiki.depasquale.persistence.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity(
    tableName = "bag_prices",
    foreignKeys = [
        ForeignKey(
            entity = Flight::class,
            parentColumns = ["id"],
            childColumns = ["parent"],
            onDelete = ForeignKey.CASCADE
        )
    ]
)
data class BagPrice(
    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = "parent") val parent: String,
    @ColumnInfo(name = "hand") val hand: Float,
    @ColumnInfo(name = "one") val first: Float,
    @ColumnInfo(name = "two") val second: Float
)

