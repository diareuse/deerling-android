package wiki.depasquale.persistence.model

import androidx.room.*
import java.util.*

@Entity(
    tableName = "destinations",
    foreignKeys = [
        ForeignKey(
            entity = Flight::class,
            parentColumns = ["id"],
            childColumns = ["parent"],
            onDelete = ForeignKey.CASCADE
        ),
    ],
    indices = [
        Index("parent")
    ]
)
data class Destination(
    @PrimaryKey
    @ColumnInfo(name = "id") val id: String,
    @ColumnInfo(name = "fly_from") val flyFrom: String,
    @ColumnInfo(name = "fly_to") val flyTo: String,
    @ColumnInfo(name = "city_from") val cityFrom: String,
    @ColumnInfo(name = "city_to") val cityTo: String,
    @ColumnInfo(name = "departure") val departure: Date,
    @ColumnInfo(name = "arrival") val arrival: Date,
    @ColumnInfo(name = "airline") val airline: String,
    @ColumnInfo(name = "flight_no") val flightNo: Int,
    @ColumnInfo(name = "lat_from") val latFrom: Double,
    @ColumnInfo(name = "lng_from") val lngFrom: Double,
    @ColumnInfo(name = "lat_to") val latTo: Double,
    @ColumnInfo(name = "lng_to") val lngTo: Double,
    @ColumnInfo(name = "last_seen") val lastSeen: Date,
    @ColumnInfo(name = "refresh_timestamp") val refreshTimestamp: Date,
    @ColumnInfo(name = "vehicle_type") val vehicleType: String,
    @ColumnInfo(name = "parent") val parent: String
)