package wiki.depasquale.persistence.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity(tableName = "flights")
data class Flight(
    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = "id") val id: String,
    @ColumnInfo(name = "fly_from") val flyFrom: String,
    @ColumnInfo(name = "fly_to") val flyTo: String,
    @ColumnInfo(name = "city_from") val cityFrom: String,
    @ColumnInfo(name = "city_to") val cityTo: String,
    @ColumnInfo(name = "country_from") val countryFrom: String,
    @ColumnInfo(name = "country_to") val countryTo: String,
    @ColumnInfo(name = "departure_time") val departure: Date,
    @ColumnInfo(name = "arrival_time") val arrival: Date,
    @ColumnInfo(name = "nights_in_destination") val nightsInDest: Int?,
    @ColumnInfo(name = "distance") val distance: Float,
    @ColumnInfo(name = "fly_duration") val flyDuration: String,
    @ColumnInfo(name = "price") val price: Float,
    @ColumnInfo(name = "price_original") val priceOriginal: Float,
    @ColumnInfo(name = "deep_link") val deepLink: String?,
    @ColumnInfo(name = "hashtags") val hashtags: List<String>,
    @ColumnInfo(name = "mapIdFrom") val mapIdFrom: String,
    @ColumnInfo(name = "mapIdTo") val mapIdTo: String
)
