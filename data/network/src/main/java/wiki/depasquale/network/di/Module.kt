package wiki.depasquale.network.di

import org.koin.dsl.module
import wiki.depasquale.network.createLoggingInterceptor
import wiki.depasquale.network.createMoshi
import wiki.depasquale.network.createOkHttp
import wiki.depasquale.network.createService
import wiki.depasquale.network.task.GetFlights

val networkModule = module {

    // direct internal dependencies
    single { createLoggingInterceptor() }
    single { createOkHttp(get()) }
    single { createMoshi() }
    single { createService(get(), get()) }

    // public dependencies
    factory { GetFlights(get()) }

}
