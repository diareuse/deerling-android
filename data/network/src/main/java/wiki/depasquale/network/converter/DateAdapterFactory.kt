package wiki.depasquale.network.converter

import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.JsonReader
import com.squareup.moshi.JsonWriter
import java.util.*

internal class DateAdapterFactory : JsonAdapter<Date>() {

    override fun fromJson(reader: JsonReader): Date {
        val type = reader.peek()
        require(type == JsonReader.Token.NUMBER) {
            "Date requires number, got ${type.name}"
        }
        val dateSeconds = reader.nextLong()
        val dateMillis = dateSeconds * 1000
        return Date(dateMillis)
    }

    override fun toJson(writer: JsonWriter, value: Date?) {
        writer.value(value?.time?.div(1000))
    }

}