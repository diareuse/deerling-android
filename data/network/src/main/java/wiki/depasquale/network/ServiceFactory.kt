package wiki.depasquale.network

import com.squareup.moshi.Moshi
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.create
import wiki.depasquale.network.converter.DateAdapterFactory
import wiki.depasquale.network.interceptor.TimberLoggingInterceptor
import java.util.*

internal fun createLoggingInterceptor() =
    TimberLoggingInterceptor()

internal fun createOkHttp(interceptor: HttpLoggingInterceptor) = OkHttpClient.Builder()
    .addInterceptor(interceptor)
    .build()

internal fun createMoshi() = Moshi.Builder()
    .add(Date::class.java, DateAdapterFactory())
    .build()

internal fun createService(
    moshi: Moshi,
    client: OkHttpClient
): Service = Retrofit.Builder()
    .client(client)
    .baseUrl(Service.Url)
    .addConverterFactory(MoshiConverterFactory.create(moshi))
    .build()
    .create()
