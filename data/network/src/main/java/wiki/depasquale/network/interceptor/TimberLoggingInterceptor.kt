package wiki.depasquale.network.interceptor

import okhttp3.logging.HttpLoggingInterceptor
import timber.log.Timber

@Suppress("FunctionName")
internal fun TimberLoggingInterceptor() = HttpLoggingInterceptor {
    Timber.tag("Network").v(it)
}.also {
    it.level = HttpLoggingInterceptor.Level.BODY
}