package wiki.depasquale.network

import retrofit2.http.GET
import retrofit2.http.QueryMap
import wiki.depasquale.network.model.FlightsResponse

internal interface Service {

    @GET("/flights")
    suspend fun getFlights(@QueryMap args: Map<String, String>): FlightsResponse

    companion object {

        const val Url = "https://api.skypicker.com/"

    }

}