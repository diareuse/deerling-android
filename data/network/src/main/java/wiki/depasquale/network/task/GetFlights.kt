package wiki.depasquale.network.task

import com.skoumal.grimoire.talisman.UseCase
import wiki.depasquale.network.Service
import wiki.depasquale.network.model.FlightsResponse
import java.text.SimpleDateFormat
import java.util.*

class GetFlights internal constructor(
    private val service: Service
) : UseCase<GetFlights.Params, FlightsResponse> {

    override suspend fun use(input: Params): FlightsResponse {
        return service.getFlights(input.asQueryMap())
    }

    // ---

    data class Params(
        private val children: Int,
        private val infants: Int,
        private val adults: Int,

        private val dateFrom: Date,
        private val dateTo: Date,
        private val type: String,

        private val latitude: Double,
        private val longitude: Double,
        private val radius: Double,
        private val to: String = "anywhere",

        private val version: String = "3",
        private val sort: String = "popularity",
        private val locale: String = Locale.getDefault().language,
        private val limit: Int = 5,
        private val featureName: String = "aggregateResults",
        private val onePerDate: Boolean = false,
        private val oneForCity: Boolean = true,
        private val partner: String = "skypicker-android"
    ) {

        private val flyFrom: String
            get() {
                // LMAO I literally cannot pass too precise coordinates, the server returns 422,
                return "%.5f-%.5f-%.0fkm".format(latitude, longitude, radius)
            }

        private fun formatDate(date: Date): String {
            val formatter = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault())
            return formatter.format(date)
        }

        private fun Boolean.asInt() = when (this) {
            true -> 1
            false -> 0
        }

        fun asQueryMap(): Map<String, String> = mutableMapOf(
            "children" to children.toString(),
            "infants" to infants.toString(),
            "adults" to adults.toString(),
            "dateFrom" to formatDate(dateFrom),
            "dateTo" to formatDate(dateFrom),
            "typeFlight" to type,

            "flyFrom" to flyFrom,
            "to" to to,

            "v" to version,
            "sort" to sort,
            "locale" to locale,
            "limit" to limit.toString(),
            "featureName" to featureName,
            "one_per_date" to onePerDate.asInt().toString(),
            "oneforcity" to oneForCity.asInt().toString(),
            "partner" to partner,
        )

    }

}