package wiki.depasquale.network.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Availability(
    @Json(name = "seats") val seats: Int?
)

