package wiki.depasquale.network.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import java.util.*

@JsonClass(generateAdapter = true)
data class Destination(
    @Json(name = "id") val id: String,
    @Json(name = "flyFrom") val flyFrom: String,
    @Json(name = "flyTo") val flyTo: String,
    @Json(name = "cityFrom") val cityFrom: String,
    @Json(name = "cityTo") val cityTo: String,
    @Json(name = "dTimeUTC") val departure: Date,
    @Json(name = "aTimeUTC") val arrival: Date,
    @Json(name = "airline") val airline: String,
    @Json(name = "flight_no") val flightNo: Int,
    @Json(name = "latFrom") val latFrom: Double,
    @Json(name = "lngFrom") val lngFrom: Double,
    @Json(name = "latTo") val latTo: Double,
    @Json(name = "lngTo") val lngTo: Double,
    @Json(name = "last_seen") val lastSeen: Date,
    @Json(name = "refresh_timestamp") val refreshTimestamp: Date,
    @Json(name = "vehicle_type") val vehicleType: String
)