package wiki.depasquale.network.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Country(
    @Json(name = "code") val code: String,
    @Json(name = "name") val name: String
)

