package wiki.depasquale.network.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Bags(
    @Json(name = "hand") val hand: Float?,
    @Json(name = "1") val first: Float?,
    @Json(name = "2") val second: Float?
)

