package wiki.depasquale.network.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class FlightsResponse(
    @Json(name = "currency") val currency: String,
    @Json(name = "data") val data: List<Flight>
)