package wiki.depasquale.network.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Discount(
    @Json(name = "original_price") val originalPrice: Float?
)
