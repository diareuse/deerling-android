package wiki.depasquale.network.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import java.util.*

@JsonClass(generateAdapter = true)
data class Flight(
    @Json(name = "id") val id: String,
    @Json(name = "flyFrom") val flyFrom: String,
    @Json(name = "flyTo") val flyTo: String,
    @Json(name = "cityFrom") val cityFrom: String,
    @Json(name = "cityTo") val cityTo: String,
    @Json(name = "countryFrom") val countryFrom: Country,
    @Json(name = "countryTo") val countryTo: Country,
    @Json(name = "dTimeUTC") val departure: Date,
    @Json(name = "aTimeUTC") val arrival: Date,
    @Json(name = "nightsInDest") val nightsInDest: Int?,
    @Json(name = "distance") val distance: Float,
    @Json(name = "fly_duration") val flyDuration: String,
    @Json(name = "price") val price: Float,
    @Json(name = "discount_data") val discountData: Discount,
    @Json(name = "bags_price") val bagsPrice: Bags,
    @Json(name = "availability") val availability: Availability,
    @Json(name = "routes") val routes: List<List<String>>,
    @Json(name = "route") val route: List<Destination>,
    @Json(name = "deep_link") val deepLink: String?,
    @Json(name = "hashtags") val hashtags: List<String>,
    @Json(name = "mapIdfrom") val mapIdFrom: String,
    @Json(name = "mapIdto") val mapIdTo: String
)
