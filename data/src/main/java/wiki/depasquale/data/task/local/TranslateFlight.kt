package wiki.depasquale.data.task.local

import com.skoumal.grimoire.talisman.UseCase
import wiki.depasquale.network.model.*
import wiki.depasquale.persistence.model.BagPrice
import wiki.depasquale.persistence.model.Route
import wiki.depasquale.persistence.task.Insert
import wiki.depasquale.persistence.model.Availability as PersistentAvailability
import wiki.depasquale.persistence.model.Country as PersistentCountry
import wiki.depasquale.persistence.model.Destination as PersistentDestination
import wiki.depasquale.persistence.model.Flight as PersistentFlight

internal class TranslateFlight internal constructor() : UseCase<Flight, Insert.Params> {

    override suspend fun use(input: Flight): Insert.Params {
        return Insert.Params(
            flight = input.asPersistent(),
            bagPrice = input.bagsPrice.asPersistent(input),
            countries = listOf(
                input.countryFrom.asPersistent(),
                input.countryTo.asPersistent()
            ),
            destinations = input.route.asPersistent(input),
            routes = input.routes.asPersistent(input),
            availability = input.availability.asPersistent(input)
        )
    }

    // ---

    private fun Flight.asPersistent(): PersistentFlight {
        return PersistentFlight(
            id = id,
            flyFrom = flyFrom,
            flyTo = flyTo,
            cityFrom = cityFrom,
            cityTo = cityTo,
            countryFrom = countryFrom.code,
            countryTo = countryTo.code,
            departure = departure,
            arrival = arrival,
            nightsInDest = nightsInDest,
            distance = distance,
            flyDuration = flyDuration,
            price = price,
            priceOriginal = discountData.originalPrice ?: price,
            deepLink = deepLink,
            hashtags = hashtags,
            mapIdFrom = mapIdFrom,
            mapIdTo = mapIdTo
        )
    }

    // ---

    private fun Country.asPersistent(): PersistentCountry {
        return PersistentCountry(
            code = code,
            name = name
        )
    }

    // ---

    private fun Bags.asPersistent(parent: Flight): BagPrice {
        return BagPrice(
            parent = parent.id,
            hand = hand ?: 0f,
            first = first ?: 0f,
            second = second ?: 0f
        )
    }

    // ---

    @JvmName("asPersistentDestinations")
    private fun List<Destination>.asPersistent(parent: Flight): List<PersistentDestination> {
        return map {
            PersistentDestination(
                id = it.id,
                flyFrom = it.flyFrom,
                flyTo = it.flyTo,
                cityFrom = it.cityFrom,
                cityTo = it.cityTo,
                departure = it.departure,
                arrival = it.arrival,
                airline = it.airline,
                flightNo = it.flightNo,
                latFrom = it.latFrom,
                lngFrom = it.lngFrom,
                latTo = it.latTo,
                lngTo = it.lngTo,
                lastSeen = it.lastSeen,
                refreshTimestamp = it.refreshTimestamp,
                vehicleType = it.vehicleType,
                parent = parent.id
            )
        }
    }

    // ---

    @JvmName("asPersistentRoutes")
    private fun List<List<String>>.asPersistent(parent: Flight): List<Route> {
        return map {
            Route(
                parent = parent.id,
                route = it
            )
        }
    }

    // ---

    private fun Availability.asPersistent(parent: Flight): PersistentAvailability {
        return PersistentAvailability(
            parent = parent.id,
            seats = seats ?: 0
        )
    }

}