package wiki.depasquale.data.task

import com.skoumal.grimoire.talisman.UseCaseFlow
import com.skoumal.grimoire.talisman.observe
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.*
import wiki.depasquale.data.model.FlightContainer
import wiki.depasquale.persistence.model.*
import wiki.depasquale.persistence.task.*
import wiki.depasquale.data.model.Availability as DataAvailability
import wiki.depasquale.data.model.BagPrice as DataBagPrice
import wiki.depasquale.data.model.Country as DataCountry
import wiki.depasquale.data.model.Destination as DataDestination
import wiki.depasquale.data.model.Flight as DataFlight
import wiki.depasquale.data.model.Route as DataRoute

class GetFlights internal constructor(
    private val flights: SelectFlights,
    private val availability: SelectAvailability,
    private val bagPrice: SelectBagPrice,
    private val country: SelectCountry,
    private val destinations: SelectDestinations,
    private val routes: SelectRoutes
) : UseCaseFlow<CoroutineScope, List<FlightContainer>> {

    override fun flow(input: CoroutineScope): Flow<List<FlightContainer>> {
        return flights.observe()
            .mapLatest { it.asContainers(input) }
    }

    // ---

    private fun List<Flight>.asContainers(scope: CoroutineScope): List<FlightContainer> =
        map { it.asContainer(scope) }

    private fun Flight.asContainer(scope: CoroutineScope): FlightContainer {
        return FlightContainer(
            flight = asData(),
            countryFrom = country.observe(countryFrom)
                .map { it.asData() }
                .stateIn(scope, SharingStarted.Lazily, DataCountry.fromCode(countryFrom)),
            countryTo = country.observe(countryTo)
                .map { it.asData() }
                .stateIn(scope, SharingStarted.Lazily, DataCountry.fromCode(countryTo)),
            destinations = destinations.observe(this)
                .map { it.asData() }
                .stateIn(scope, SharingStarted.Lazily, emptyList()),
            routes = routes.observe(this)
                .map { it.asData() }
                .stateIn(scope, SharingStarted.Lazily, emptyList()),
            availability = availability.observe(this)
                .map { it.asData() }
                .stateIn(scope, SharingStarted.Lazily, DataAvailability.empty()),
            bagPrice = bagPrice.observe(this)
                .map { it.asData() }
                .stateIn(scope, SharingStarted.Lazily, DataBagPrice.empty())
        )
    }

    // ---

    private fun Flight.asData(): DataFlight {
        return DataFlight(
            id = id,
            flyFrom = flyFrom,
            flyTo = flyTo,
            cityFrom = cityFrom,
            cityTo = cityTo,
            departure = departure,
            arrival = arrival,
            nightsInDest = nightsInDest,
            distance = distance,
            flyDuration = flyDuration,
            price = price,
            priceOriginal = priceOriginal,
            deepLink = deepLink,
            hashtags = hashtags,
            mapIdFrom = mapIdFrom,
            mapIdTo = mapIdTo
        )
    }

    // ---

    private fun Country.asData(): DataCountry {
        return DataCountry(
            code = code,
            name = name
        )
    }

    // ---

    @JvmName("asDataDestinations")
    private fun List<Destination>.asData() =
        map { it.asData() }

    private fun Destination.asData(): DataDestination {
        return DataDestination(
            id = id,
            flyFrom = flyFrom,
            flyTo = flyTo,
            cityFrom = cityFrom,
            cityTo = cityTo,
            departure = departure,
            arrival = arrival,
            airline = airline,
            flightNo = flightNo,
            latFrom = latFrom,
            lngFrom = lngFrom,
            latTo = latTo,
            lngTo = lngTo,
            lastSeen = lastSeen,
            refreshTimestamp = refreshTimestamp,
            vehicleType = vehicleType
        )
    }

    // ---

    @JvmName("asDataRoutes")
    private fun List<Route>.asData() =
        map { it.asData() }

    private fun Route.asData(): DataRoute {
        return DataRoute(stops = route)
    }

    // ---

    private fun BagPrice.asData(): DataBagPrice {
        return DataBagPrice(
            hand = hand,
            first = first,
            second = second
        )
    }

    // ---

    private fun Availability.asData(): DataAvailability {
        return DataAvailability(seats = seats)
    }

}