package wiki.depasquale.data.task

import android.text.format.DateUtils.isToday
import com.skoumal.grimoire.talisman.UseCase
import com.skoumal.grimoire.talisman.invoke
import com.skoumal.grimoire.talisman.seal.getOrThrow
import com.skoumal.grimoire.talisman.seal.mapSealed
import com.skoumal.grimoire.talisman.seal.onSuccess
import wiki.depasquale.data.preferences.Preferences
import wiki.depasquale.data.task.local.TranslateFlight
import wiki.depasquale.network.model.Flight
import wiki.depasquale.network.task.GetFlights
import wiki.depasquale.persistence.task.Delete
import wiki.depasquale.persistence.task.Insert
import wiki.depasquale.persistence.task.SelectLatestDeparture
import java.util.*
import java.util.concurrent.TimeUnit

class UpdateFlights internal constructor(
    private val flights: GetFlights,
    private val delete: Delete,
    private val insert: Insert,
    private val translation: TranslateFlight,
    private val preferences: Preferences,
    private val latestDeparture: SelectLatestDeparture
) : UseCase<Unit, Unit> {

    private val today
        get() = Calendar.getInstance().time

    private val tomorrow
        get() = today + TimeUnit.DAYS.toMillis(1)

    override suspend fun use(input: Unit) {
        latestDeparture().mapSealed { requireNotNull(it) }.onSuccess {
            if (isToday(it.time)) {
                return
            }
        }

        val params = GetFlights.Params(
            children = preferences.children,
            infants = preferences.infants,
            adults = preferences.adults,

            dateFrom = today,
            dateTo = tomorrow,
            type = "oneway",

            latitude = preferences.latitude,
            longitude = preferences.longitude,
            radius = preferences.radius,
        )

        val flights = flights(params).getOrThrow()
            .also { preferences.currency = it.currency }
            .data
            .asInput()

        if (flights.isEmpty()) {
            return
        }

        delete(null).getOrThrow()
        insert(flights).getOrThrow()
    }

    // ---

    private suspend fun List<Flight>.asInput() =
        translation(this).getOrThrow().toList()

    private operator fun Date.plus(millis: Long): Date {
        return Date(time + millis)
    }

}

