package wiki.depasquale.data.preferences

interface Preferences {

    val isValid: Boolean

    var children: Int
    var infants: Int
    var adults: Int

    var latitude: Double
    var longitude: Double
    var radius: Double

    var currency: String

}