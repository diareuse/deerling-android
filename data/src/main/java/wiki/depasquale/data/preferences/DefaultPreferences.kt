package wiki.depasquale.data.preferences

import android.annotation.SuppressLint
import android.content.Context

@SuppressLint("ApplySharedPref")
class DefaultPreferences(context: Context) : Preferences {

    private val preferences =
        context.getSharedPreferences(context.packageName + ".default", Context.MODE_PRIVATE)

    // ---

    override val isValid: Boolean
        get() = latitude != 0.0 && longitude != 0.0 && radius > 0

    // ---

    override var children: Int
        get() = preferences.getInt(Key.Children.key, 0)
        set(value) {
            preferences.edit()
                .putInt(Key.Children.key, value)
                .commit()
        }

    override var infants: Int
        get() = preferences.getInt(Key.Infants.key, 0)
        set(value) {
            preferences.edit()
                .putInt(Key.Infants.key, value)
                .commit()
        }

    override var adults: Int
        get() = preferences.getInt(Key.Adults.key, 1)
        set(value) {
            preferences.edit()
                .putInt(Key.Adults.key, value)
                .commit()
        }

    override var latitude: Double
        get() = preferences.getFloat(Key.Latitude.key, 0f).toDouble()
        set(value) {
            preferences.edit()
                .putFloat(Key.Latitude.key, value.toFloat())
                .commit()
        }

    override var longitude: Double
        get() = preferences.getFloat(Key.Longitude.key, 0f).toDouble()
        set(value) {
            preferences.edit()
                .putFloat(Key.Longitude.key, value.toFloat())
                .commit()
        }

    override var radius: Double
        get() = preferences.getFloat(Key.Radius.key, 0f).toDouble()
        set(value) {
            preferences.edit()
                .putFloat(Key.Radius.key, value.toFloat())
                .commit()
        }

    override var currency: String
        get() = preferences.getString(Key.Currency.key, "") ?: ""
        set(value) {
            preferences.edit()
                .putString(Key.Currency.key, value)
                .commit()
        }

    private enum class Key(val key: String) {
        Children("4RM5DxYvsf"),
        Infants("m3N7WYgZ07"),
        Adults("y1r9z7mFKI"),
        Latitude("o6EeRtRUEe"),
        Longitude("2oIHYM6i16"),
        Radius("RxSo8LiQQD"),
        Currency("xs82U9XDkr"),
    }

}