package wiki.depasquale.data.di

import org.koin.dsl.module
import wiki.depasquale.data.preferences.DefaultPreferences
import wiki.depasquale.data.preferences.Preferences
import wiki.depasquale.data.task.GetFlights
import wiki.depasquale.data.task.UpdateFlights
import wiki.depasquale.data.task.local.TranslateFlight
import wiki.depasquale.network.di.networkModule
import wiki.depasquale.persistence.di.persistenceModule

val networkModule = networkModule
val persistenceModule = persistenceModule
val dataModule = module {
    single<Preferences> { DefaultPreferences(get()) }

    factory { TranslateFlight() }
    factory { GetFlights(get(), get(), get(), get(), get(), get()) }
    factory { UpdateFlights(get(), get(), get(), get(), get(), get()) }
}
