package wiki.depasquale.persistence.di

import org.koin.dsl.module
import wiki.depasquale.persistence.Persistence
import wiki.depasquale.persistence.task.*

val persistenceModule = module {
    single { Persistence(get()) }
    single { get<Persistence>().availability() }
    single { get<Persistence>().bagPrices() }
    single { get<Persistence>().countries() }
    single { get<Persistence>().destinations() }
    single { get<Persistence>().flights() }
    single { get<Persistence>().routes() }
    single { get<Persistence>().insertion() }

    factory { Delete(get()) }
    factory { Insert(get()) }
    factory { SelectAvailability(get()) }
    factory { SelectBagPrice(get()) }
    factory { SelectCountry(get()) }
    factory { SelectDestinations(get()) }
    factory { SelectFlights(get()) }
    factory { SelectLatestDeparture(get()) }
    factory { SelectRoutes(get()) }

}