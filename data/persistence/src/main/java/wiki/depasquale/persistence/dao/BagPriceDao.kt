package wiki.depasquale.persistence.dao

import androidx.room.Dao
import androidx.room.Query
import kotlinx.coroutines.flow.Flow
import wiki.depasquale.persistence.model.BagPrice

@Dao
internal interface BagPriceDao {

    @Query("SELECT * FROM bag_prices WHERE parent=:parent LIMIT 1")
    fun observeByParent(parent: String): Flow<BagPrice>

}