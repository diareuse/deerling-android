package wiki.depasquale.persistence.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Transaction
import wiki.depasquale.persistence.model.*

@Dao
internal abstract class InsertionDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    protected abstract suspend fun insertBagPrice(item: BagPrice)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    protected abstract suspend fun insertCountries(items: List<Country>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    protected abstract suspend fun insertDestinations(items: List<Destination>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    protected abstract suspend fun insertFlight(item: Flight)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    protected abstract suspend fun insertRoutes(items: List<Route>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    protected abstract suspend fun insertAvailability(item: Availability)

    // ---

    @Transaction
    open suspend fun insert(
        flight: Flight,
        bagPrice: BagPrice,
        countries: List<Country>,
        destinations: List<Destination>,
        routes: List<Route>,
        availability: Availability
    ) {
        insertFlight(flight)
        insertBagPrice(bagPrice)
        insertCountries(countries)
        insertDestinations(destinations)
        insertRoutes(routes)
        insertAvailability(availability)
    }

}