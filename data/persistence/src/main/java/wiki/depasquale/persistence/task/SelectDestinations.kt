package wiki.depasquale.persistence.task

import com.skoumal.grimoire.talisman.UseCaseFlow
import kotlinx.coroutines.flow.Flow
import wiki.depasquale.persistence.dao.DestinationDao
import wiki.depasquale.persistence.model.Destination
import wiki.depasquale.persistence.model.Flight

class SelectDestinations internal constructor(
    private val dao: DestinationDao
) : UseCaseFlow<Flight, List<Destination>> {

    override fun flow(input: Flight): Flow<List<Destination>> {
        return dao.observeByParent(input.id)
    }

}