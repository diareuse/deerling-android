package wiki.depasquale.persistence.task

import com.skoumal.grimoire.talisman.UseCase
import wiki.depasquale.persistence.dao.FlightDao
import wiki.depasquale.persistence.model.Flight

class Delete internal constructor(
    private val dao: FlightDao
) : UseCase<Flight?, Unit> {

    override suspend fun use(input: Flight?) {
        when (input) {
            null -> dao.deleteAll()
            else -> dao.delete(input)
        }
    }

}