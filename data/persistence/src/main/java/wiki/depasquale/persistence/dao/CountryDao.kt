package wiki.depasquale.persistence.dao

import androidx.room.Dao
import androidx.room.Query
import kotlinx.coroutines.flow.Flow
import wiki.depasquale.persistence.model.Country

@Dao
internal interface CountryDao {

    @Query("SELECT * FROM countries WHERE code=:code LIMIT 1")
    fun observe(code: String): Flow<Country>

}