package wiki.depasquale.persistence.converter

import androidx.room.TypeConverter

class StringListConverter {

    @TypeConverter
    fun fromList(list: List<String>) = list.joinToString(separator = "|")

    @TypeConverter
    fun fromString(string: String) = string.split('|')

}