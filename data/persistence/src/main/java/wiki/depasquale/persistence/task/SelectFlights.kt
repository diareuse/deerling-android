package wiki.depasquale.persistence.task

import com.skoumal.grimoire.talisman.UseCaseFlow
import kotlinx.coroutines.flow.Flow
import wiki.depasquale.persistence.dao.FlightDao
import wiki.depasquale.persistence.model.Flight

class SelectFlights internal constructor(
    private val dao: FlightDao
) : UseCaseFlow<Unit, List<Flight>> {

    override fun flow(input: Unit): Flow<List<Flight>> {
        return dao.observe()
    }

}