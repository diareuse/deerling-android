package wiki.depasquale.persistence.task

import com.skoumal.grimoire.talisman.UseCaseFlow
import kotlinx.coroutines.flow.Flow
import wiki.depasquale.persistence.dao.RouteDao
import wiki.depasquale.persistence.model.Flight
import wiki.depasquale.persistence.model.Route

class SelectRoutes internal constructor(
    private val dao: RouteDao
) : UseCaseFlow<Flight, List<Route>> {

    override fun flow(input: Flight): Flow<List<Route>> {
        return dao.observeByParent(input.id)
    }

}