package wiki.depasquale.persistence.task

import com.skoumal.grimoire.talisman.UseCase
import wiki.depasquale.persistence.dao.InsertionDao
import wiki.depasquale.persistence.model.*

class Insert internal constructor(
    private val dao: InsertionDao
) : UseCase<Insert.Params, Unit> {

    override suspend fun use(input: Params) {
        val (flight, bagPrice, countries, destinations, routes, availability) = input
        dao.insert(
            flight = flight,
            bagPrice = bagPrice,
            countries = countries,
            destinations = destinations,
            routes = routes,
            availability = availability
        )
    }

    // ---

    data class Params(
        @JvmField val flight: Flight,
        @JvmField val bagPrice: BagPrice,
        @JvmField val countries: List<Country>,
        @JvmField val destinations: List<Destination>,
        @JvmField val routes: List<Route>,
        @JvmField val availability: Availability
    )

}