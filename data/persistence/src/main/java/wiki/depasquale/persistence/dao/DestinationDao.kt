package wiki.depasquale.persistence.dao

import androidx.room.Dao
import androidx.room.Query
import kotlinx.coroutines.flow.Flow
import wiki.depasquale.persistence.model.Destination

@Dao
internal interface DestinationDao {

    @Query("SELECT * FROM destinations WHERE parent=:parent")
    fun observeByParent(parent: String): Flow<List<Destination>>

}