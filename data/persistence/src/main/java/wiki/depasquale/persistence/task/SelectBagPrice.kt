package wiki.depasquale.persistence.task

import com.skoumal.grimoire.talisman.UseCaseFlow
import kotlinx.coroutines.flow.Flow
import wiki.depasquale.persistence.dao.BagPriceDao
import wiki.depasquale.persistence.model.BagPrice
import wiki.depasquale.persistence.model.Flight

class SelectBagPrice internal constructor(
    private val dao: BagPriceDao
) : UseCaseFlow<Flight, BagPrice> {

    override fun flow(input: Flight): Flow<BagPrice> {
        return dao.observeByParent(input.id)
    }

}



