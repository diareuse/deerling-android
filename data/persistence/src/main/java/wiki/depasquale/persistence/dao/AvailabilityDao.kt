package wiki.depasquale.persistence.dao

import androidx.room.Dao
import androidx.room.Query
import kotlinx.coroutines.flow.Flow
import wiki.depasquale.persistence.model.Availability

@Dao
interface AvailabilityDao {

    @Query("SELECT * FROM availabilities WHERE parent=:parent LIMIT 1")
    fun observeByParent(parent: String): Flow<Availability>

}