package wiki.depasquale.persistence.dao

import androidx.room.Dao
import androidx.room.Query
import kotlinx.coroutines.flow.Flow
import wiki.depasquale.persistence.model.Route

@Dao
internal interface RouteDao {

    @Query("SELECT * FROM routes WHERE parent=:parent")
    fun observeByParent(parent: String): Flow<List<Route>>

}