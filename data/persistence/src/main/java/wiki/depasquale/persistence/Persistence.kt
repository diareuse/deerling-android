package wiki.depasquale.persistence

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import wiki.depasquale.persistence.converter.DateConverter
import wiki.depasquale.persistence.converter.StringListConverter
import wiki.depasquale.persistence.dao.*
import wiki.depasquale.persistence.model.*

@Database(
    version = 1,
    entities = [
        Availability::class,
        BagPrice::class,
        Country::class,
        Flight::class,
        Route::class,
        Destination::class
    ],
)
@TypeConverters(
    DateConverter::class,
    StringListConverter::class
)
internal abstract class Persistence : RoomDatabase() {

    abstract fun availability(): AvailabilityDao
    abstract fun bagPrices(): BagPriceDao
    abstract fun countries(): CountryDao
    abstract fun destinations(): DestinationDao
    abstract fun flights(): FlightDao
    abstract fun routes(): RouteDao
    abstract fun insertion(): InsertionDao

    companion object {

        operator fun invoke(context: Context) = Room
            .databaseBuilder(context, Persistence::class.java, context.packageName + ".persistence")
            .fallbackToDestructiveMigration()
            .build()

    }

}