package wiki.depasquale.persistence.task

import com.skoumal.grimoire.talisman.UseCaseFlow
import kotlinx.coroutines.flow.Flow
import wiki.depasquale.persistence.dao.AvailabilityDao
import wiki.depasquale.persistence.model.Availability
import wiki.depasquale.persistence.model.Flight

class SelectAvailability internal constructor(
    private val dao: AvailabilityDao
) : UseCaseFlow<Flight, Availability> {

    override fun flow(input: Flight): Flow<Availability> {
        return dao.observeByParent(input.id)
    }

}