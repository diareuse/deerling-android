package wiki.depasquale.persistence.task

import com.skoumal.grimoire.talisman.UseCaseFlow
import kotlinx.coroutines.flow.Flow
import wiki.depasquale.persistence.dao.CountryDao
import wiki.depasquale.persistence.model.Country

class SelectCountry internal constructor(
    private val dao: CountryDao
) : UseCaseFlow<String, Country> {

    override fun flow(input: String): Flow<Country> {
        return dao.observe(input)
    }

}