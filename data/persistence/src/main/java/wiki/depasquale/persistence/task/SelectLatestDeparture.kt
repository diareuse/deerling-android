package wiki.depasquale.persistence.task

import com.skoumal.grimoire.talisman.UseCase
import wiki.depasquale.persistence.dao.FlightDao
import java.util.*

class SelectLatestDeparture internal constructor(
    private val dao: FlightDao
) : UseCase<Unit, Date?> {
    override suspend fun use(input: Unit): Date? {
        return dao.selectLatestDeparture()
    }
}