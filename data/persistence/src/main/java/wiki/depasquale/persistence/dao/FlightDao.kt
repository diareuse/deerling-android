package wiki.depasquale.persistence.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Query
import kotlinx.coroutines.flow.Flow
import wiki.depasquale.persistence.model.Flight
import java.util.*

@Dao
internal interface FlightDao {

    @Query("SELECT * FROM flights ORDER BY (price_original - price) DESC")
    fun observe(): Flow<List<Flight>>

    @Query("SELECT departure_time FROM flights ORDER BY departure_time DESC LIMIT 1")
    fun selectLatestDeparture(): Date?

    @Delete
    suspend fun delete(item: Flight)

    @Query("DELETE FROM flights")
    suspend fun deleteAll()

}