package wiki.depasquale.data.model

import kotlinx.coroutines.flow.StateFlow

class FlightContainer(
    val flight: Flight,
    val countryFrom: StateFlow<Country>,
    val countryTo: StateFlow<Country>,
    val destinations: StateFlow<List<Destination>>,
    val routes: StateFlow<List<Route>>,
    val availability: StateFlow<Availability>,
    val bagPrice: StateFlow<BagPrice>
)