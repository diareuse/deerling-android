package wiki.depasquale.data.model

data class Availability(
    val seats: Int
) {

    companion object {

        fun empty() = Availability(seats = 0)

    }

}