package wiki.depasquale.data.model

data class BagPrice(
    val hand: Float,
    val first: Float,
    val second: Float
) {

    companion object {

        fun empty() = BagPrice(
            hand = 0f,
            first = 0f,
            second = 0f
        )

    }

}