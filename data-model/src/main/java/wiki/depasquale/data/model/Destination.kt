package wiki.depasquale.data.model

import java.util.*

data class Destination(
    val id: String,
    val flyFrom: String,
    val flyTo: String,
    val cityFrom: String,
    val cityTo: String,
    val departure: Date,
    val arrival: Date,
    val airline: String,
    val flightNo: Int,
    val latFrom: Double,
    val lngFrom: Double,
    val latTo: Double,
    val lngTo: Double,
    val lastSeen: Date,
    val refreshTimestamp: Date,
    val vehicleType: String
)