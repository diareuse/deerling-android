package wiki.depasquale.data.model

data class Country(
    val code: String,
    val name: String
) {

    companion object {

        fun fromCode(code: String) = Country(
            code = code,
            name = code
        )

    }

}