package wiki.depasquale.data.model

import java.text.DateFormat
import java.util.*

data class Flight(
    val id: String,
    val flyFrom: String,
    val flyTo: String,
    val cityFrom: String,
    val cityTo: String,
    val departure: Date,
    val arrival: Date,
    val nightsInDest: Int?,
    val distance: Float,
    val flyDuration: String,
    val price: Float,
    val priceOriginal: Float,
    val deepLink: String?,
    val hashtags: List<String>,
    val mapIdFrom: String,
    val mapIdTo: String
) {

    val imageFrom
        get() = TemplateImage.format(mapIdFrom)

    val imageTo
        get() = TemplateImage.format(mapIdTo)

    val departureTime
        get() = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT).format(departure)

    val arrivalTime
        get() = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT).format(arrival)

    val hashtagsString
        get() = hashtags.filterNot { it.contains(':') }.joinToString()

    companion object {

        private const val TemplateImage = "https://images.kiwi.com/photos/600x330/%s.jpg"

    }

}
