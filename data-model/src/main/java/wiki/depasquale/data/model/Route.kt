package wiki.depasquale.data.model

data class Route(
    val stops: List<String>
)